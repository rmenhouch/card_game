(function ($) {
    $.fn.orderCards = function () {
        swal.queue([{
                title: 'REARRANGE CARDS',
                text: 'You are about to sort cards automatically.',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                confirmButtonText:
                        '<i class="fa fa-thumbs-up"></i> Go, AUTO SORT',
                type: 'info',
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.getJSON("sort")
                                .done(function (json) {
                                    location.reload();
                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    var err = textStatus + ", " + error;
                                    console.log("Request Failed: " + err);
                                });
                    })
                }
            }]).catch(swal.noop);
    };

    $.fn.retrieveCards = function () {
        swal.queue([{
                title: 'You are Asking for cards',
                confirmButtonText: 'Pull Cards',
                text: 'Click to continue.',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.getJSON("retrieve")
                                .done(function (json) {
                                    swal.insertQueueStep('You are ready ! \nExerciceId: ' + json.exerciceId)
                                    resolve()
                                    window.setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                })
                                .fail(function (jqxhr, textStatus, error) {
                                    var err = textStatus + ", " + error;
                                    console.log("Request Failed: " + err);
                                });
                    })
                }
            }]).catch(swal.noop);
    };

    $.fn.verifyCards = function () {


        swal({
            title: 'Are you sure?',
            text: "You are sending cards to server!",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Verify!',
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    $.ajax({
                        url: 'verify',
                        type: 'POST',
                        dataType: 'json'
                    })
                            .done(function (response) {
                                swal('Finished !', response.message, response.status);
                            })
                            .fail(function () {
                                swal('Oops...', 'Something went wrong with ajax !', 'error');
                            });
                });
            },
            allowOutsideClick: false
        });
    };

    $('a#get_cards_btn, a#refresh_cards_btn').click(function () {
        (!$(this).attr("disabled")) && $(this).retrieveCards();
    });

    $('a#order_cards_btn').click(function () {
        (!$(this).attr("disabled")) && $(this).orderCards();
    });

    $('a#verify_cards_btn').click(function () {
        (!$(this).attr("disabled")) && $(this).verifyCards();
    });


})(jQuery);
