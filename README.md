The PROJECT
===========

A Symfony project created on Jan 20, 2018, 2:31 pm.
The application is responsible for sorting 10 cards in given category/values order.

Result can be verified using remote server.

DEVELOPMENT ENVIRONMENT
=======================

The application is based on **Symfony 3.2** and deployed under **PHP 7.1.**

REQUIREMENTS
============

* PHP minimum version 5.5.9
* JSON extension needs to be enabled
* ctype extension needs to be enabled
* CURL extension needs to be enabled
* Your php.ini needs to have the date.timezone setting

MANUAL DEPLOYMENT METHOD
========================

1. Upload files (including /vendor directory) via FTP/SFTP.
2. Browse to http://server.tld/project_name/web/  - assuming your files   
   are contained in project_name directory.

GIT/COMPOSER DEPLOYMENT METHOD
==========================

In this method, you will clone project files from git repository, then install the dependencies & symfony via composer:

* Fom Web root, run the following commands:

```
#!SHELL

git clone https://rmenhouch@bitbucket.org/rmenhouch/card_game.git project_name
$ cd project_name/
$ composer install
```

* Browse to http://server.tld/project_name/web/ (assuming your files   
   are contained in project_name directory).
