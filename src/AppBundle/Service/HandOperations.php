<?php
/*
 * This Service is Reponsible for operations on cards (sort, verify).
 */

namespace AppBundle\Service;

class HandOperations {

    /**
     * This Script will auto-sort cards to requested order
     * You just Watch, I play !! HuH...No drag & drop until hired  
     * @param stdClass $obj
     * @return stdClass
     */
    public function sort($obj) {
        list($handCards, $categoryOrder, $valueOrder) = array($obj->data->cards, $obj->data->categoryOrder, $obj->data->valueOrder);

        // Create complete  array with categories as keys and values order as values, keeping the same order
        // Eg. array('HEART' => array('ACE' => false, 'TWO' => false...) )
        $arrCards = array();
        foreach ($categoryOrder as $category) {
            $arrCards[$category] = array_fill_keys($valueOrder, false);
        }

        //Fill the previously created array with corresponding card in iteration.
        foreach ($handCards as $card) {
            $arrCards[$card->category][$card->value] = $card;
        }

        //Clean array, keep only available cards.
        $arrSortedCards = array();
        foreach ($arrCards as $category => $values) {
            foreach ($values as $card) {
                if (is_object($card)) {
                    array_push($arrSortedCards, $card);
                }
            }
        }

        $obj->data->cards = $arrSortedCards;
        $obj->data->handSorted = true;
        return $obj;
    }

    /**
     * Verifies cards sorting
     * 
     * @param stdClass $obj
     * @return mixed
     */
    public function verify($obj) {
        list($exerciceId, $cards) = array($obj->exerciceId, $obj->data->cards);

        $verificationUrl = 'https://recrutement.local-trust.com/test/' . $exerciceId;
        $strCards = json_encode(array('cards' => $cards, 'categoryOrder' => $obj->data->categoryOrder, 'valueOrder' => $obj->data->valueOrder));
        
        /*
         * Wrong answer test:
         * To simulate wrong answer, please uncomment the following line. 
         */
        #$strCards = '{"cards":[{"category":"DIAMOND","value":"TEN"},{"category":"DIAMOND","value":"QUEEN"},{"category":"DIAMOND","value":"KING"},{"category":"HEART","value":"EIGHT"},{"category":"HEART","value":"NINE"},{"category":"SPADE","value":"FIVE"},{"category":"SPADE","value":"SEVEN"},{"category":"SPADE","value":"KING"},{"category":"CLUB","value":"ACE"},{"category":"CLUB","value":"TWO"}],"categoryOrder":["DIAMOND","HEART","SPADE","CLUB"],"valueOrder":["ACE","TWO","THREE","FOUR","FIVE","SIX","SEVEN","EIGHT","NINE","TEN","JACK","QUEEN","KING"]}';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $verificationUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $strCards);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_exec($ch);
        // Check HTTP status code
        $response = array('message' => 'NOT FOUND', 'status' => 'error');
        if (!curl_errno($ch)) {
            switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                case 200:  # CORRECT
                    $response = array('message' => 'CORRECT ANSWER', 'status' => 'success');
                    break;
                case 406:  # WRONG
                    $response = array('message' => 'WRONG ANSWER', 'status' => 'error');
                    break;
                default:
                    $response = array('message' => 'Unexpected HTTP error ', $http_code, "\n", 'status' => 'error');
            }
        }
        return $response;
    }

}
