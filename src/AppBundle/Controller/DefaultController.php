<?php
namespace AppBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller {

    /**
     * Read object from session and prepares data to the view
     * 
     * 
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
            
        $objStored = $request->getSession()->get('card_hand');
        return $this->render('default/index.html.twig', [
                    'exercice_id' => $objStored ? $objStored->exerciceId : 0,
                    'player_name' => $objStored ? $objStored->candidate->firstName : 'Guest',
                    'cards' => $objStored ? $objStored->data->cards : [],
                    'rules' => $objStored ? $objStored->data : []
        ]);
    }

    /**
     * Retrieve 10 Cards from remote server, and write persistent object in session
     * 
     * @Route("/retrieve", name="retrieve")
     */
    public function retrieveAction(Request $request) {
        if ($request->isXmlHttpRequest()) {
            $objSrvResponse = json_decode(file_get_contents('https://recrutement.local-trust.com/test/cards/586f4e7f975adeb8520a4b88'));
            $session = $request->getSession();
            $objSrvResponse->data->handSorted = false;
            $session->set('card_hand', $objSrvResponse);
            return new JsonResponse($objSrvResponse);
        }
        return new JsonResponse('no results found', Response::HTTP_NOT_FOUND); // constant for 404
    }
    
    /**
     * Sort Cards based on requested order
     * 
     * @Route("/sort", name="sort")
     */
    public function sortAction(Request $request) {
        $session = $request->getSession();
        $objStored = $session->get('card_hand');
        if ($request->isXmlHttpRequest() && $objStored) {
            $objSorted = $this->get('app.hand_operations')->sort($objStored);
            $session->set('card_hand', $objSorted);
            return new JsonResponse($objSorted);
        }
        return new JsonResponse('no results found', Response::HTTP_NOT_FOUND); // constant for 404
    }

    /**
     * Verify cards order status 
     * 
     * @Route("/verify", name="verify")
     */
    public function verifyAction(Request $request) {
        $session = $request->getSession();
        $objStored = $session->get('card_hand');
        if ($request->isXmlHttpRequest() && $objStored) {
            $objSrvResponse = $this->get('app.hand_operations')->verify($objStored);
            return new JsonResponse($objSrvResponse);
        }
        return new JsonResponse('no results found', Response::HTTP_NOT_FOUND); // constant for 404
    }
}
