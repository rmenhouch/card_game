<?php

/* default/index.html.twig */
class __TwigTemplate_45c4b012f0452bba40cabd851dd14369190a74925e41f8a7a31ef5a4bf188800 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b0607b8bac2da11c60cf759a1d21c3d05f9fefbce8589661609215a093f3edf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b0607b8bac2da11c60cf759a1d21c3d05f9fefbce8589661609215a093f3edf->enter($__internal_0b0607b8bac2da11c60cf759a1d21c3d05f9fefbce8589661609215a093f3edf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0b0607b8bac2da11c60cf759a1d21c3d05f9fefbce8589661609215a093f3edf->leave($__internal_0b0607b8bac2da11c60cf759a1d21c3d05f9fefbce8589661609215a093f3edf_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_824859deaaf3865135deb95475fe6cf86c2d81f2e970fbfdaca477a6e0070fd0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_824859deaaf3865135deb95475fe6cf86c2d81f2e970fbfdaca477a6e0070fd0->enter($__internal_824859deaaf3865135deb95475fe6cf86c2d81f2e970fbfdaca477a6e0070fd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div id=\"wrapper\">
        <div id=\"container\">
            ";
        // line 5
        $this->loadTemplate("::header.html.twig", "default/index.html.twig", 5)->display($context);
        // line 6
        echo "            <!-- Indicators
            ================================================== -->
            <div class=\"bs-docs-section\">
                <div class=\"row\">
                    <div class=\"col-lg-12\">
                        ";
        // line 11
        if (twig_test_empty((isset($context["cards"]) ? $context["cards"] : $this->getContext($context, "cards")))) {
            // line 12
            echo "
                            <div class=\"bs-component\">
                                <div class=\"alert alert-dismissible alert-warning\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                                    <h4> Your need to retrieve 10 cards !</h4>
                                    <p>Please click on the <b>GET 10 CARDS</b> button to continue.</p>
                                </div>
                            </div>
                        ";
        } else {
            // line 21
            echo "                           ";
            if (($this->getAttribute((isset($context["rules"]) ? $context["rules"] : $this->getContext($context, "rules")), "handSorted", array()) == false)) {
                // line 22
                echo "                            <div class=\"bs-component\">
                                <div class=\"alert alert-dismissible alert-success\" role=\"alert\">
                                    <strong>Good Luck !</strong>
                                    <p>To sort cards, please click the <b>AUTO SORT</b> button.</p>
                                    <p>If you need to match results with excercice solution, please click the <b>VERIFY</b> button.</p>
                                </div>
                            </div>
                           ";
            } elseif (($this->getAttribute(            // line 29
(isset($context["rules"]) ? $context["rules"] : $this->getContext($context, "rules")), "handSorted", array()) == true)) {
                // line 30
                echo "                                <div class=\"bs-component\">
                                    <div class=\"alert alert-dismissible alert-warning\">
                                        <strong>Sorting done !!</strong>
                                        <p><img class=\"sign-image\" src=\"";
                // line 33
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/signs/DOLLAR.png"), "html", null, true);
                echo "\" /> Cards are Sorted according to required order.</p>
                                        <p>If you need to match results with excercice solution, please click the <b>VERIFY</b> button.</p>
                                    </div>
                                </div>
                            ";
            }
            // line 38
            echo "                            <div class=\"sign-container row centered text-center\">
                                ";
            // line 39
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["rules"]) ? $context["rules"] : $this->getContext($context, "rules")), "categoryOrder", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 40
                echo "                                    <div class=\"col-lg-3 col-md-3 col-sm-3\">
                                        <img class=\"sign-image\" src=\"";
                // line 41
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl((("images/signs/" . $context["category"]) . ".png")), "html", null, true);
                echo "\" />
                                    </div>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "                                <small class=\"hidden-xs\">";
            echo twig_escape_filter($this->env, twig_join_filter($this->getAttribute((isset($context["rules"]) ? $context["rules"] : $this->getContext($context, "rules")), "valueOrder", array()), " >> "), "html", null, true);
            echo "</small>
                            </div>
                            <div class=\"row cards-container\">
                                <div class=\"cards-btn\">
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"order_cards_btn\" ";
            // line 49
            if (($this->getAttribute((isset($context["rules"]) ? $context["rules"] : $this->getContext($context, "rules")), "handSorted", array()) == true)) {
                echo " disabled ";
            }
            echo " class=\"btn btn-danger\"><i class=\"fa fa-heart\"></i> AUTO SORT</a>
                                    </div>
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"verify_cards_btn\" ";
            // line 52
            if (($this->getAttribute((isset($context["rules"]) ? $context["rules"] : $this->getContext($context, "rules")), "handSorted", array()) == false)) {
                echo " disabled ";
            }
            echo " class=\"btn btn-warning\"><i class=\"fa fa-check\"></i> VERIFY ORDER</a>
                                    </div>
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"refresh_cards_btn\" ";
            // line 55
            if (twig_test_empty((isset($context["cards"]) ? $context["cards"] : $this->getContext($context, "cards")))) {
                echo " disabled ";
            }
            echo " class=\"btn btn-info\"><i class=\"fa fa-refresh\"></i> RESTART</a>
                                    </div>
                                    <div class=\"clearfix\"></div>
                                </div>
                                <div>
                                    ";
            // line 60
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["cards"]) ? $context["cards"] : $this->getContext($context, "cards")));
            foreach ($context['_seq'] as $context["_key"] => $context["card"]) {
                // line 61
                echo "                                        <div class=\"card-container col-lg-2 col-md-5 col-sm-3\">
                                            <img class=\"card-image\" src=\"";
                // line 62
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl((((("images/cards/" . $this->getAttribute($context["card"], "category", array())) . "/") . $this->getAttribute($context["card"], "value", array())) . ".png")), "html", null, true);
                echo "\" />
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['card'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "                                </div>
                            </div>
                        ";
        }
        // line 68
        echo "                    </div>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_824859deaaf3865135deb95475fe6cf86c2d81f2e970fbfdaca477a6e0070fd0->leave($__internal_824859deaaf3865135deb95475fe6cf86c2d81f2e970fbfdaca477a6e0070fd0_prof);

    }

    // line 74
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_25c261870dbbf0540e177a546d8588d06876d21c6771d9ea49b93012d3f023ba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25c261870dbbf0540e177a546d8588d06876d21c6771d9ea49b93012d3f023ba->enter($__internal_25c261870dbbf0540e177a546d8588d06876d21c6771d9ea49b93012d3f023ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 75
        echo "
    ";
        // line 76
        $this->loadTemplate("::footer.html.twig", "default/index.html.twig", 76)->display($context);
        
        $__internal_25c261870dbbf0540e177a546d8588d06876d21c6771d9ea49b93012d3f023ba->leave($__internal_25c261870dbbf0540e177a546d8588d06876d21c6771d9ea49b93012d3f023ba_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 76,  187 => 75,  181 => 74,  169 => 68,  164 => 65,  155 => 62,  152 => 61,  148 => 60,  138 => 55,  130 => 52,  122 => 49,  113 => 44,  104 => 41,  101 => 40,  97 => 39,  94 => 38,  86 => 33,  81 => 30,  79 => 29,  70 => 22,  67 => 21,  56 => 12,  54 => 11,  47 => 6,  45 => 5,  41 => 3,  35 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block body %}
    <div id=\"wrapper\">
        <div id=\"container\">
            {% include '::header.html.twig' %}
            <!-- Indicators
            ================================================== -->
            <div class=\"bs-docs-section\">
                <div class=\"row\">
                    <div class=\"col-lg-12\">
                        {% if cards is empty %}

                            <div class=\"bs-component\">
                                <div class=\"alert alert-dismissible alert-warning\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                                    <h4> Your need to retrieve 10 cards !</h4>
                                    <p>Please click on the <b>GET 10 CARDS</b> button to continue.</p>
                                </div>
                            </div>
                        {% else %}
                           {% if rules.handSorted == false %}
                            <div class=\"bs-component\">
                                <div class=\"alert alert-dismissible alert-success\" role=\"alert\">
                                    <strong>Good Luck !</strong>
                                    <p>To sort cards, please click the <b>AUTO SORT</b> button.</p>
                                    <p>If you need to match results with excercice solution, please click the <b>VERIFY</b> button.</p>
                                </div>
                            </div>
                           {% elseif  rules.handSorted == true %}
                                <div class=\"bs-component\">
                                    <div class=\"alert alert-dismissible alert-warning\">
                                        <strong>Sorting done !!</strong>
                                        <p><img class=\"sign-image\" src=\"{{ asset('images/signs/DOLLAR.png') }}\" /> Cards are Sorted according to required order.</p>
                                        <p>If you need to match results with excercice solution, please click the <b>VERIFY</b> button.</p>
                                    </div>
                                </div>
                            {% endif %}
                            <div class=\"sign-container row centered text-center\">
                                {% for category in rules.categoryOrder %}
                                    <div class=\"col-lg-3 col-md-3 col-sm-3\">
                                        <img class=\"sign-image\" src=\"{{ asset('images/signs/'~category~'.png') }}\" />
                                    </div>
                                {% endfor %}
                                <small class=\"hidden-xs\">{{ rules.valueOrder|join(' >> ') }}</small>
                            </div>
                            <div class=\"row cards-container\">
                                <div class=\"cards-btn\">
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"order_cards_btn\" {% if rules.handSorted == true %} disabled {% endif %} class=\"btn btn-danger\"><i class=\"fa fa-heart\"></i> AUTO SORT</a>
                                    </div>
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"verify_cards_btn\" {% if rules.handSorted == false %} disabled {% endif %} class=\"btn btn-warning\"><i class=\"fa fa-check\"></i> VERIFY ORDER</a>
                                    </div>
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"refresh_cards_btn\" {% if cards is  empty %} disabled {% endif %} class=\"btn btn-info\"><i class=\"fa fa-refresh\"></i> RESTART</a>
                                    </div>
                                    <div class=\"clearfix\"></div>
                                </div>
                                <div>
                                    {% for card in cards %}
                                        <div class=\"card-container col-lg-2 col-md-5 col-sm-3\">
                                            <img class=\"card-image\" src=\"{{ asset('images/cards/'~card.category~'/'~card.value~'.png') }}\" />
                                        </div>
                                    {% endfor %}
                                </div>
                            </div>
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
{% block javascripts  %}

    {% include '::footer.html.twig' %}
{% endblock %}
", "default/index.html.twig", "C:\\wamp\\www\\cards\\app\\Resources\\views\\default\\index.html.twig");
    }
}
