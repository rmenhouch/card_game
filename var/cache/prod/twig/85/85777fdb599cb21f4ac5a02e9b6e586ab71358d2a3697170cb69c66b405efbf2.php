<?php

/* ::footer.html.twig */
class __TwigTemplate_ed0819a9d2787c2b064c00091cdd8297cf62d25a98115453dde68edd30417c16 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script src=\"https://code.jquery.com/jquery.min.js\"></script>
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js\"></script>

<script>

    (function (\$) {

        \$.fn.orderCards = function () {
            swal.queue([{
                    title: 'REARRANGE CARDS',
                    text: 'You are about to sort cards automatically.',
                    showLoaderOnConfirm: true,
                    showCancelButton: true,
                    confirmButtonText:
                            '<i class=\"fa fa-thumbs-up\"></i> Go, AUTO SORT',
                    type: 'info',
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            \$.getJSON(\"sort\")
                                    .done(function (json) {
                                        location.reload();
                                    })
                                    .fail(function (jqxhr, textStatus, error) {
                                        var err = textStatus + \", \" + error;
                                        console.log(\"Request Failed: \" + err);
                                    });
                        })
                    }
                }]).catch(swal.noop);
        };

        \$.fn.retrieveCards = function () {
            swal.queue([{
                    title: 'You are Asking for cards',
                    confirmButtonText: 'Pull Cards',
                    text: 'Click to continue.',
                    showLoaderOnConfirm: true,
                    showCancelButton: true,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            \$.getJSON(\"retrieve\")
                                    .done(function (json) {
                                        swal.insertQueueStep('You are ready ! \\nExerciceId: ' + json.exerciceId)
                                        resolve()
                                        window.setTimeout(function () {
                                            location.reload();
                                        }, 1000);
                                    })
                                    .fail(function (jqxhr, textStatus, error) {
                                        var err = textStatus + \", \" + error;
                                        console.log(\"Request Failed: \" + err);
                                    });
                        })
                    }
                }]).catch(swal.noop);
        };

        \$('a#get_cards_btn, a#refresh_cards_btn').click(function () {
            (!\$(this).attr(\"disabled\")) && \$(this).retrieveCards();
        });

        \$('a#order_cards_btn').click(function () {
            (!\$(this).attr(\"disabled\")) && \$(this).orderCards();
        });


    })(jQuery);

</script>
";
    }

    public function getTemplateName()
    {
        return "::footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "::footer.html.twig", "C:\\wamp\\www\\cards\\app/Resources\\views/footer.html.twig");
    }
}
