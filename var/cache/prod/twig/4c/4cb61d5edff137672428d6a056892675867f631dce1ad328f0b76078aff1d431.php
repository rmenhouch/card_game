<?php

/* base.html.twig */
class __TwigTemplate_6f6978cb0bb13379ba7b44c5f88a4b3a677b7bcea63f7a447528db8d51a75b5b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cb589bd0f65c9df9823a1d4aa826c9d8d978e89e4d8159e3bfafb025b55c5d24 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb589bd0f65c9df9823a1d4aa826c9d8d978e89e4d8159e3bfafb025b55c5d24->enter($__internal_cb589bd0f65c9df9823a1d4aa826c9d8d978e89e4d8159e3bfafb025b55c5d24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_cb589bd0f65c9df9823a1d4aa826c9d8d978e89e4d8159e3bfafb025b55c5d24->leave($__internal_cb589bd0f65c9df9823a1d4aa826c9d8d978e89e4d8159e3bfafb025b55c5d24_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_a46e078d92c1a4749ead5abf58caaefcb403d7ac3a86c6a201aed5bae7af1baf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a46e078d92c1a4749ead5abf58caaefcb403d7ac3a86c6a201aed5bae7af1baf->enter($__internal_a46e078d92c1a4749ead5abf58caaefcb403d7ac3a86c6a201aed5bae7af1baf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "G@@D Luck!";
        
        $__internal_a46e078d92c1a4749ead5abf58caaefcb403d7ac3a86c6a201aed5bae7af1baf->leave($__internal_a46e078d92c1a4749ead5abf58caaefcb403d7ac3a86c6a201aed5bae7af1baf_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_4c14080e33927b053e1634a11f75d6d5e3b46c68234fe511d3405687d9559021 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c14080e33927b053e1634a11f75d6d5e3b46c68234fe511d3405687d9559021->enter($__internal_4c14080e33927b053e1634a11f75d6d5e3b46c68234fe511d3405687d9559021_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_4c14080e33927b053e1634a11f75d6d5e3b46c68234fe511d3405687d9559021->leave($__internal_4c14080e33927b053e1634a11f75d6d5e3b46c68234fe511d3405687d9559021_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_0112096290e8020c92c5c1b538961666da2466cc3a369379ed581095239a962c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0112096290e8020c92c5c1b538961666da2466cc3a369379ed581095239a962c->enter($__internal_0112096290e8020c92c5c1b538961666da2466cc3a369379ed581095239a962c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_0112096290e8020c92c5c1b538961666da2466cc3a369379ed581095239a962c->leave($__internal_0112096290e8020c92c5c1b538961666da2466cc3a369379ed581095239a962c_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_5050a7291fd41e43ee0c7edb4421767fb4add4fdce022d843f44f48b00e47535 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5050a7291fd41e43ee0c7edb4421767fb4add4fdce022d843f44f48b00e47535->enter($__internal_5050a7291fd41e43ee0c7edb4421767fb4add4fdce022d843f44f48b00e47535_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_5050a7291fd41e43ee0c7edb4421767fb4add4fdce022d843f44f48b00e47535->leave($__internal_5050a7291fd41e43ee0c7edb4421767fb4add4fdce022d843f44f48b00e47535_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}G@@D Luck!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "C:\\wamp\\www\\cards\\app\\Resources\\views\\base.html.twig");
    }
}
