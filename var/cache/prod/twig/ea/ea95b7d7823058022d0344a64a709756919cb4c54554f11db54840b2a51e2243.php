<?php

/* base.html.twig */
class __TwigTemplate_6e4f8f28e11b93606fa8e4708c8f15b36e673002b8b3bee101923fa9a138e851 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7632eb038cf39fd6f481ff8497f53e207a9ea94224249e7f0c2bba9ab0571198 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7632eb038cf39fd6f481ff8497f53e207a9ea94224249e7f0c2bba9ab0571198->enter($__internal_7632eb038cf39fd6f481ff8497f53e207a9ea94224249e7f0c2bba9ab0571198_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_7632eb038cf39fd6f481ff8497f53e207a9ea94224249e7f0c2bba9ab0571198->leave($__internal_7632eb038cf39fd6f481ff8497f53e207a9ea94224249e7f0c2bba9ab0571198_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_c5a88706901a09c5a1123de7a83be5aeb2eff37e0653c089ca0aeca3ce3c83ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c5a88706901a09c5a1123de7a83be5aeb2eff37e0653c089ca0aeca3ce3c83ad->enter($__internal_c5a88706901a09c5a1123de7a83be5aeb2eff37e0653c089ca0aeca3ce3c83ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "G@@D Luck!";
        
        $__internal_c5a88706901a09c5a1123de7a83be5aeb2eff37e0653c089ca0aeca3ce3c83ad->leave($__internal_c5a88706901a09c5a1123de7a83be5aeb2eff37e0653c089ca0aeca3ce3c83ad_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_010a5c1506864351430307d26313c088659930d675672d2624376e780af6a974 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_010a5c1506864351430307d26313c088659930d675672d2624376e780af6a974->enter($__internal_010a5c1506864351430307d26313c088659930d675672d2624376e780af6a974_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_010a5c1506864351430307d26313c088659930d675672d2624376e780af6a974->leave($__internal_010a5c1506864351430307d26313c088659930d675672d2624376e780af6a974_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_2c659040d4828dcddced81723e40534167be66007586ad8c5d32a98c005fe0c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c659040d4828dcddced81723e40534167be66007586ad8c5d32a98c005fe0c2->enter($__internal_2c659040d4828dcddced81723e40534167be66007586ad8c5d32a98c005fe0c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_2c659040d4828dcddced81723e40534167be66007586ad8c5d32a98c005fe0c2->leave($__internal_2c659040d4828dcddced81723e40534167be66007586ad8c5d32a98c005fe0c2_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_db6c0e600828f3810a62c14e9446f8a8eaa830a1e1d84d976c4f412f4311a34c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db6c0e600828f3810a62c14e9446f8a8eaa830a1e1d84d976c4f412f4311a34c->enter($__internal_db6c0e600828f3810a62c14e9446f8a8eaa830a1e1d84d976c4f412f4311a34c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_db6c0e600828f3810a62c14e9446f8a8eaa830a1e1d84d976c4f412f4311a34c->leave($__internal_db6c0e600828f3810a62c14e9446f8a8eaa830a1e1d84d976c4f412f4311a34c_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}G@@D Luck!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "C:\\wamp\\www\\cards\\app\\Resources\\views\\base.html.twig");
    }
}
