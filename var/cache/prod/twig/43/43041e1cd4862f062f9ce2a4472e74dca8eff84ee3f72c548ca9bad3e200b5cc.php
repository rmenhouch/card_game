<?php

/* ::header.html.twig */
class __TwigTemplate_2494aced8260a405c8e745a3beefd34b614716ee5264e7964386927d9c690037 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_18f94bf9a02a1d8e1abc2b1a28e0fea0b72c3afa5ad88b94a42f2062e3aa03ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18f94bf9a02a1d8e1abc2b1a28e0fea0b72c3afa5ad88b94a42f2062e3aa03ab->enter($__internal_18f94bf9a02a1d8e1abc2b1a28e0fea0b72c3afa5ad88b94a42f2062e3aa03ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::header.html.twig"));

        // line 1
        echo "<div class=\"row\">
    <div class=\"col-lg-8 col-md-7 col-sm-6\">
        <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.css\" rel=\"stylesheet\" type=\"text/css\" />
        <link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/override.css"), "html", null, true);
        echo "\" />
        <h1><i class='fa fa-user'></i> Welcome ";
        // line 7
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, (isset($context["player_name"]) ? $context["player_name"] : $this->getContext($context, "player_name"))), "html", null, true);
        echo "</h1>
        <span class=\"text-muted\">EXCERICE ID: ";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["exercice_id"]) ? $context["exercice_id"] : $this->getContext($context, "exercice_id")), "html", null, true);
        echo "</span>
    </div>

    <div class=\"col-lg-4 col-md-5 col-sm-6\">
        <div class=\"sponsor\">
            <a id=\"get_cards_btn\" ";
        // line 13
        if ( !twig_test_empty((isset($context["cards"]) ? $context["cards"] : $this->getContext($context, "cards")))) {
            echo " disabled ";
        }
        echo " class=\"btn btn-lg btn-rounded btn-block btn-primary\">GET 10 CARDS <i class=\"fa fa-magic\"></i></a>
        </div>
    </div>
</div>
<hr/>
";
        // line 18
        if (twig_test_empty((isset($context["cards"]) ? $context["cards"] : $this->getContext($context, "cards")))) {
            // line 19
            echo "    <div class=\"row\">
        <div class=\"col-lg-12\">
            <div class=\"page-header\">
                <h1 id=\"indicators\"><span class=\"label label-warning\"><i class=\"fa fa-warning\"></i> Hand empty: 0 card</span></h1>
            </div>
        </div>
    </div>
";
        } else {
            // line 27
            echo "    <div class=\"row\">
        <div class=\"col-lg-12\">
            <div class=\"page-header\">
                <h1 id=\"indicators\"><span class=\"label label-success\"><i class=\"fa fa-hand-o-right\"></i> Hand:  ";
            // line 30
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["cards"]) ? $context["cards"] : $this->getContext($context, "cards"))), "html", null, true);
            echo " cards</span></h1>
            </div>
        </div>
    </div>   
                   
";
        }
        
        $__internal_18f94bf9a02a1d8e1abc2b1a28e0fea0b72c3afa5ad88b94a42f2062e3aa03ab->leave($__internal_18f94bf9a02a1d8e1abc2b1a28e0fea0b72c3afa5ad88b94a42f2062e3aa03ab_prof);

    }

    public function getTemplateName()
    {
        return "::header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 30,  67 => 27,  57 => 19,  55 => 18,  45 => 13,  37 => 8,  33 => 7,  29 => 6,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"row\">
    <div class=\"col-lg-8 col-md-7 col-sm-6\">
        <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.css\" rel=\"stylesheet\" type=\"text/css\" />
        <link rel=\"stylesheet\" href=\"{{ asset('css/override.css') }}\" />
        <h1><i class='fa fa-user'></i> Welcome {{ player_name|capitalize }}</h1>
        <span class=\"text-muted\">EXCERICE ID: {{ exercice_id }}</span>
    </div>

    <div class=\"col-lg-4 col-md-5 col-sm-6\">
        <div class=\"sponsor\">
            <a id=\"get_cards_btn\" {% if cards is not empty %} disabled {% endif %} class=\"btn btn-lg btn-rounded btn-block btn-primary\">GET 10 CARDS <i class=\"fa fa-magic\"></i></a>
        </div>
    </div>
</div>
<hr/>
{% if cards is empty %}
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <div class=\"page-header\">
                <h1 id=\"indicators\"><span class=\"label label-warning\"><i class=\"fa fa-warning\"></i> Hand empty: 0 card</span></h1>
            </div>
        </div>
    </div>
{% else %}
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <div class=\"page-header\">
                <h1 id=\"indicators\"><span class=\"label label-success\"><i class=\"fa fa-hand-o-right\"></i> Hand:  {{ cards|length }} cards</span></h1>
            </div>
        </div>
    </div>   
                   
{% endif %}", "::header.html.twig", "C:\\wamp\\www\\cards\\app\\Resources\\views\\header.html.twig");
    }
}
