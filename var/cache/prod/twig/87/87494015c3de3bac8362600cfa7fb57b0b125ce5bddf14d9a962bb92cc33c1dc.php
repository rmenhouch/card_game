<?php

/* ::footer.html.twig */
class __TwigTemplate_67c9222353860a85426dfa58ebd36b08ed90c2a9363cd3e79bf5160e3f2cdd8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5991602cdd8e1f692bac7c768c9eb5ba86b8dcd7506f42d35503fa5da77ac5b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5991602cdd8e1f692bac7c768c9eb5ba86b8dcd7506f42d35503fa5da77ac5b1->enter($__internal_5991602cdd8e1f692bac7c768c9eb5ba86b8dcd7506f42d35503fa5da77ac5b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::footer.html.twig"));

        // line 1
        echo "<script src=\"https://code.jquery.com/jquery.min.js\"></script>
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js\"></script>
<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>

";
        
        $__internal_5991602cdd8e1f692bac7c768c9eb5ba86b8dcd7506f42d35503fa5da77ac5b1->leave($__internal_5991602cdd8e1f692bac7c768c9eb5ba86b8dcd7506f42d35503fa5da77ac5b1_prof);

    }

    public function getTemplateName()
    {
        return "::footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<script src=\"https://code.jquery.com/jquery.min.js\"></script>
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js\"></script>
<script src=\"{{asset('js/app.js')}}\"></script>

", "::footer.html.twig", "C:\\wamp\\www\\cards\\app\\Resources\\views\\footer.html.twig");
    }
}
