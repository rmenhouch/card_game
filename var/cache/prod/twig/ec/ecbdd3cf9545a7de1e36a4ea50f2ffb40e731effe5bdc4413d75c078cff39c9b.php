<?php

/* default/index.html.twig */
class __TwigTemplate_f66b40fbd6ee0211a198a7ae0e3697005e7c56e7a7f3a9e784b750b79e88e62c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"wrapper\">
        <div id=\"container\">
            ";
        // line 5
        $this->loadTemplate("::header.html.twig", "default/index.html.twig", 5)->display($context);
        // line 6
        echo "            <!-- Indicators
            ================================================== -->
            <div class=\"bs-docs-section\">
                <div class=\"row\">
                    <div class=\"col-lg-12\">
                        ";
        // line 11
        if (twig_test_empty(($context["cards"] ?? null))) {
            // line 12
            echo "
                            <div class=\"bs-component\">
                                <div class=\"alert alert-dismissible alert-warning\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                                    <h4> Your need to retrieve 10 cards !</h4>
                                    <p>Please click on the <b>GET 10 CARDS</b> button to continue.</p>
                                </div>
                            </div>
                        ";
        } else {
            // line 21
            echo "                           ";
            if (($this->getAttribute(($context["rules"] ?? null), "handSorted", array()) == false)) {
                // line 22
                echo "                            <div class=\"bs-component\">
                                <div class=\"alert alert-dismissible alert-success\" role=\"alert\">
                                    <strong>Good Luck !</strong>
                                    <p>To sort cards, please click the <b>AUTO SORT</b> button.</p>
                                    <p>If you need to match results with excercice solution, please click the <b>VERIFY</b> button.</p>
                                </div>
                            </div>
                           ";
            } elseif (($this->getAttribute(            // line 29
($context["rules"] ?? null), "handSorted", array()) == true)) {
                // line 30
                echo "                                <div class=\"bs-component\">
                                    <div class=\"alert alert-dismissible alert-warning\">
                                        <strong>Sorting done !!</strong>
                                        <p><img class=\"sign-image\" src=\"";
                // line 33
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/signs/DOLLAR.png"), "html", null, true);
                echo "\" /> Cards are Sorted according to required order.</p>
                                        <p>If you need to match results with excercice solution, please click the <b>VERIFY</b> button.</p>
                                    </div>
                                </div>
                            ";
            }
            // line 38
            echo "                            <div class=\"sign-container row centered text-center\">
                                ";
            // line 39
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["rules"] ?? null), "categoryOrder", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 40
                echo "                                    <div class=\"col-lg-3 col-md-3 col-sm-3\">
                                        <img class=\"sign-image\" src=\"";
                // line 41
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl((("images/signs/" . $context["category"]) . ".png")), "html", null, true);
                echo "\" />
                                    </div>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "                                <small class=\"hidden-xs\">";
            echo twig_escape_filter($this->env, twig_join_filter($this->getAttribute(($context["rules"] ?? null), "valueOrder", array()), " >> "), "html", null, true);
            echo "</small>
                            </div>
                            <div class=\"row cards-container\">
                                <div class=\"cards-btn\">
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"order_cards_btn\" ";
            // line 49
            if (($this->getAttribute(($context["rules"] ?? null), "handSorted", array()) == true)) {
                echo " disabled ";
            }
            echo " class=\"btn btn-danger\"><i class=\"fa fa-heart\"></i> AUTO SORT</a>
                                    </div>
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"refresh_cards_btn\" ";
            // line 52
            if (twig_test_empty(($context["cards"] ?? null))) {
                echo " disabled ";
            }
            echo " class=\"btn btn-info\"><i class=\"fa fa-refresh\"></i> RESTART</a>
                                    </div>
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"verify_cards_btn\" ";
            // line 55
            if (($this->getAttribute(($context["rules"] ?? null), "handSorted", array()) == false)) {
                echo " disabled ";
            }
            echo " class=\"btn btn-warning\"><i class=\"fa fa-check\"></i> VERIFY ORDER</a>
                                    </div>
                                    <div class=\"clearfix\"></div>
                                </div>
                                <div>
                                    ";
            // line 60
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["cards"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["card"]) {
                // line 61
                echo "                                        <div class=\"card-container col-lg-2 col-md-5 col-sm-3\">
                                            <img class=\"card-image\" src=\"";
                // line 62
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl((((("images/cards/" . $this->getAttribute($context["card"], "category", array())) . "/") . $this->getAttribute($context["card"], "value", array())) . ".png")), "html", null, true);
                echo "\" />
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['card'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "                                </div>
                            </div>
                        ";
        }
        // line 68
        echo "                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 74
    public function block_javascripts($context, array $blocks = array())
    {
        // line 75
        echo "
    ";
        // line 76
        $this->loadTemplate("::footer.html.twig", "default/index.html.twig", 76)->display($context);
    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 76,  172 => 75,  169 => 74,  160 => 68,  155 => 65,  146 => 62,  143 => 61,  139 => 60,  129 => 55,  121 => 52,  113 => 49,  104 => 44,  95 => 41,  92 => 40,  88 => 39,  85 => 38,  77 => 33,  72 => 30,  70 => 29,  61 => 22,  58 => 21,  47 => 12,  45 => 11,  38 => 6,  36 => 5,  32 => 3,  29 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/index.html.twig", "C:\\wamp\\www\\cards\\app\\Resources\\views\\default\\index.html.twig");
    }
}
