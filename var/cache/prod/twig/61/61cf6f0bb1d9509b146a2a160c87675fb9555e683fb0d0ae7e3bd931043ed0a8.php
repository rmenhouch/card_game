<?php

/* ::header.html.twig */
class __TwigTemplate_eb34dba033fb61d79d93180e96c630f39d43ee398f110b98d464d51182390f8b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row\">
    <div class=\"col-lg-8 col-md-7 col-sm-6\">
        <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.css\" rel=\"stylesheet\" type=\"text/css\" />
        <link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/override.css"), "html", null, true);
        echo "\" />
        <h1><i class='fa fa-user'></i> Welcome ";
        // line 7
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, ($context["player_name"] ?? null)), "html", null, true);
        echo "</h1>
        <span class=\"text-muted\">EXCERICE ID: ";
        // line 8
        echo twig_escape_filter($this->env, ($context["exercice_id"] ?? null), "html", null, true);
        echo "</span>
    </div>

    <div class=\"col-lg-4 col-md-5 col-sm-6\">
        <div class=\"sponsor\">
            <a id=\"get_cards_btn\" ";
        // line 13
        if ( !twig_test_empty(($context["cards"] ?? null))) {
            echo " disabled ";
        }
        echo " class=\"btn btn-lg btn-rounded btn-block btn-primary\">GET 10 CARDS <i class=\"fa fa-magic\"></i></a>
        </div>
    </div>
</div>
<hr/>
";
        // line 18
        if (twig_test_empty(($context["cards"] ?? null))) {
            // line 19
            echo "    <div class=\"row\">
        <div class=\"col-lg-12\">
            <div class=\"page-header\">
                <h1 id=\"indicators\"><span class=\"label label-warning\"><i class=\"fa fa-warning\"></i> Hand empty: 0 card</span></h1>
            </div>
        </div>
    </div>
";
        } else {
            // line 27
            echo "    <div class=\"row\">
        <div class=\"col-lg-12\">
            <div class=\"page-header\">
                <h1 id=\"indicators\"><span class=\"label label-success\"><i class=\"fa fa-hand-o-right\"></i> Hand:  ";
            // line 30
            echo twig_escape_filter($this->env, twig_length_filter($this->env, ($context["cards"] ?? null)), "html", null, true);
            echo " cards</span></h1>
            </div>
        </div>
    </div>   
                   
";
        }
    }

    public function getTemplateName()
    {
        return "::header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 30,  64 => 27,  54 => 19,  52 => 18,  42 => 13,  34 => 8,  30 => 7,  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "::header.html.twig", "C:\\wamp\\www\\cards\\app\\Resources\\views\\header.html.twig");
    }
}
