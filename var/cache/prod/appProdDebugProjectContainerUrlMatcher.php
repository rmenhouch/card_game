<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        // retrieve
        if ($pathinfo === '/retrieve') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::retrieveAction',  '_route' => 'retrieve',);
        }

        // sort
        if ($pathinfo === '/sort') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::sortAction',  '_route' => 'sort',);
        }

        // verify
        if ($pathinfo === '/verify') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::verifyAction',  '_route' => 'verify',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
