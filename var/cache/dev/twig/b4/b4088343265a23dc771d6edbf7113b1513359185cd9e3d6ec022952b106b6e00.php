<?php

/* base.html.twig */
class __TwigTemplate_8b9af363f79570f54baa8c6ff66ed3fd291fa90ae054c39ca27af4c2c64f332b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a9bd4cdd5f071583ca0bae24a695bcbcfeefba51504fdb08b01f1bf7e66d13df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9bd4cdd5f071583ca0bae24a695bcbcfeefba51504fdb08b01f1bf7e66d13df->enter($__internal_a9bd4cdd5f071583ca0bae24a695bcbcfeefba51504fdb08b01f1bf7e66d13df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_9866fcadfb95637e06841895d7f1a8d73a81c27f69c64092af8e2b947818c2bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9866fcadfb95637e06841895d7f1a8d73a81c27f69c64092af8e2b947818c2bc->enter($__internal_9866fcadfb95637e06841895d7f1a8d73a81c27f69c64092af8e2b947818c2bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_a9bd4cdd5f071583ca0bae24a695bcbcfeefba51504fdb08b01f1bf7e66d13df->leave($__internal_a9bd4cdd5f071583ca0bae24a695bcbcfeefba51504fdb08b01f1bf7e66d13df_prof);

        
        $__internal_9866fcadfb95637e06841895d7f1a8d73a81c27f69c64092af8e2b947818c2bc->leave($__internal_9866fcadfb95637e06841895d7f1a8d73a81c27f69c64092af8e2b947818c2bc_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_2211c0e9ae84c7f3fb76d121f3f0b6a5ab3330b47c10adb239b38eb660570f2f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2211c0e9ae84c7f3fb76d121f3f0b6a5ab3330b47c10adb239b38eb660570f2f->enter($__internal_2211c0e9ae84c7f3fb76d121f3f0b6a5ab3330b47c10adb239b38eb660570f2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_4d10d724f78e5e01ef970457cd772b7d6a7d83486c18733736791426cad0a453 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d10d724f78e5e01ef970457cd772b7d6a7d83486c18733736791426cad0a453->enter($__internal_4d10d724f78e5e01ef970457cd772b7d6a7d83486c18733736791426cad0a453_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "G@@D Luck!";
        
        $__internal_4d10d724f78e5e01ef970457cd772b7d6a7d83486c18733736791426cad0a453->leave($__internal_4d10d724f78e5e01ef970457cd772b7d6a7d83486c18733736791426cad0a453_prof);

        
        $__internal_2211c0e9ae84c7f3fb76d121f3f0b6a5ab3330b47c10adb239b38eb660570f2f->leave($__internal_2211c0e9ae84c7f3fb76d121f3f0b6a5ab3330b47c10adb239b38eb660570f2f_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_3600e3be3d0b4e9ab05892f2cc47e5934e436aa693b151b1c55638509ea70d6d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3600e3be3d0b4e9ab05892f2cc47e5934e436aa693b151b1c55638509ea70d6d->enter($__internal_3600e3be3d0b4e9ab05892f2cc47e5934e436aa693b151b1c55638509ea70d6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_92761edb494a5c8c0f204e65439c400775c384bb4e59284aa75b00fd0ac28df0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92761edb494a5c8c0f204e65439c400775c384bb4e59284aa75b00fd0ac28df0->enter($__internal_92761edb494a5c8c0f204e65439c400775c384bb4e59284aa75b00fd0ac28df0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_92761edb494a5c8c0f204e65439c400775c384bb4e59284aa75b00fd0ac28df0->leave($__internal_92761edb494a5c8c0f204e65439c400775c384bb4e59284aa75b00fd0ac28df0_prof);

        
        $__internal_3600e3be3d0b4e9ab05892f2cc47e5934e436aa693b151b1c55638509ea70d6d->leave($__internal_3600e3be3d0b4e9ab05892f2cc47e5934e436aa693b151b1c55638509ea70d6d_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_79d8528bad1790c1430db13b018672bda9b85df827944c766fdee554a25788fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79d8528bad1790c1430db13b018672bda9b85df827944c766fdee554a25788fc->enter($__internal_79d8528bad1790c1430db13b018672bda9b85df827944c766fdee554a25788fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_de2b26cf83bd260e07b177fce3876293ce05430cd3a528941f89a9e83d9e13cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de2b26cf83bd260e07b177fce3876293ce05430cd3a528941f89a9e83d9e13cf->enter($__internal_de2b26cf83bd260e07b177fce3876293ce05430cd3a528941f89a9e83d9e13cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_de2b26cf83bd260e07b177fce3876293ce05430cd3a528941f89a9e83d9e13cf->leave($__internal_de2b26cf83bd260e07b177fce3876293ce05430cd3a528941f89a9e83d9e13cf_prof);

        
        $__internal_79d8528bad1790c1430db13b018672bda9b85df827944c766fdee554a25788fc->leave($__internal_79d8528bad1790c1430db13b018672bda9b85df827944c766fdee554a25788fc_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_3fdd7c1784bc63b35841d800c6d58350a982ea98a37639feeb777ae037f54ac4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3fdd7c1784bc63b35841d800c6d58350a982ea98a37639feeb777ae037f54ac4->enter($__internal_3fdd7c1784bc63b35841d800c6d58350a982ea98a37639feeb777ae037f54ac4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_729a1a434d7ba02f68d14c753ea32bdd633727275d2b5ba9507a663ba1273716 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_729a1a434d7ba02f68d14c753ea32bdd633727275d2b5ba9507a663ba1273716->enter($__internal_729a1a434d7ba02f68d14c753ea32bdd633727275d2b5ba9507a663ba1273716_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_729a1a434d7ba02f68d14c753ea32bdd633727275d2b5ba9507a663ba1273716->leave($__internal_729a1a434d7ba02f68d14c753ea32bdd633727275d2b5ba9507a663ba1273716_prof);

        
        $__internal_3fdd7c1784bc63b35841d800c6d58350a982ea98a37639feeb777ae037f54ac4->leave($__internal_3fdd7c1784bc63b35841d800c6d58350a982ea98a37639feeb777ae037f54ac4_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}G@@D Luck!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "C:\\wamp\\www\\cards\\app\\Resources\\views\\base.html.twig");
    }
}
