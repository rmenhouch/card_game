<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_11a9aa5207848cfa5529e5877d033d599202ce5db8285cf4d41c1c3aeca6b710 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c6f1cbc9104469eadccb2a5af94659ee664cd3513298e205a218e851d96bd7dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6f1cbc9104469eadccb2a5af94659ee664cd3513298e205a218e851d96bd7dc->enter($__internal_c6f1cbc9104469eadccb2a5af94659ee664cd3513298e205a218e851d96bd7dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_a4eb5f76e634fa2aa3f944636df71907c4543dbd4b9262f6ceca95e2d344e612 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4eb5f76e634fa2aa3f944636df71907c4543dbd4b9262f6ceca95e2d344e612->enter($__internal_a4eb5f76e634fa2aa3f944636df71907c4543dbd4b9262f6ceca95e2d344e612_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c6f1cbc9104469eadccb2a5af94659ee664cd3513298e205a218e851d96bd7dc->leave($__internal_c6f1cbc9104469eadccb2a5af94659ee664cd3513298e205a218e851d96bd7dc_prof);

        
        $__internal_a4eb5f76e634fa2aa3f944636df71907c4543dbd4b9262f6ceca95e2d344e612->leave($__internal_a4eb5f76e634fa2aa3f944636df71907c4543dbd4b9262f6ceca95e2d344e612_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_2cf3da001b241fed04d9da0a9fb08fce22903c799a9c4168e090c24a575b03cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2cf3da001b241fed04d9da0a9fb08fce22903c799a9c4168e090c24a575b03cc->enter($__internal_2cf3da001b241fed04d9da0a9fb08fce22903c799a9c4168e090c24a575b03cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_4be7fe998bbdb2e714215ae4015d14f45384add9faad0b35889b87a79e0db670 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4be7fe998bbdb2e714215ae4015d14f45384add9faad0b35889b87a79e0db670->enter($__internal_4be7fe998bbdb2e714215ae4015d14f45384add9faad0b35889b87a79e0db670_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_4be7fe998bbdb2e714215ae4015d14f45384add9faad0b35889b87a79e0db670->leave($__internal_4be7fe998bbdb2e714215ae4015d14f45384add9faad0b35889b87a79e0db670_prof);

        
        $__internal_2cf3da001b241fed04d9da0a9fb08fce22903c799a9c4168e090c24a575b03cc->leave($__internal_2cf3da001b241fed04d9da0a9fb08fce22903c799a9c4168e090c24a575b03cc_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_1298f21cbe8b7b27ce879615416559da501f6895dc6883a5088e519ce95c3b77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1298f21cbe8b7b27ce879615416559da501f6895dc6883a5088e519ce95c3b77->enter($__internal_1298f21cbe8b7b27ce879615416559da501f6895dc6883a5088e519ce95c3b77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_c9ee83880ee24aa4323192e1b11e53082cdf9e0521ac420ab1b13583ec051f9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c9ee83880ee24aa4323192e1b11e53082cdf9e0521ac420ab1b13583ec051f9a->enter($__internal_c9ee83880ee24aa4323192e1b11e53082cdf9e0521ac420ab1b13583ec051f9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_c9ee83880ee24aa4323192e1b11e53082cdf9e0521ac420ab1b13583ec051f9a->leave($__internal_c9ee83880ee24aa4323192e1b11e53082cdf9e0521ac420ab1b13583ec051f9a_prof);

        
        $__internal_1298f21cbe8b7b27ce879615416559da501f6895dc6883a5088e519ce95c3b77->leave($__internal_1298f21cbe8b7b27ce879615416559da501f6895dc6883a5088e519ce95c3b77_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_ae0123ce02207024af3c54fecd0874079866c2a3ead670b6c0f85ae85a78afbf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae0123ce02207024af3c54fecd0874079866c2a3ead670b6c0f85ae85a78afbf->enter($__internal_ae0123ce02207024af3c54fecd0874079866c2a3ead670b6c0f85ae85a78afbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1e3f52977d9e96f56d130a8840efa97e031f5289676ecac6bbab3457d013be0b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e3f52977d9e96f56d130a8840efa97e031f5289676ecac6bbab3457d013be0b->enter($__internal_1e3f52977d9e96f56d130a8840efa97e031f5289676ecac6bbab3457d013be0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_1e3f52977d9e96f56d130a8840efa97e031f5289676ecac6bbab3457d013be0b->leave($__internal_1e3f52977d9e96f56d130a8840efa97e031f5289676ecac6bbab3457d013be0b_prof);

        
        $__internal_ae0123ce02207024af3c54fecd0874079866c2a3ead670b6c0f85ae85a78afbf->leave($__internal_ae0123ce02207024af3c54fecd0874079866c2a3ead670b6c0f85ae85a78afbf_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "C:\\wamp\\www\\cards\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
