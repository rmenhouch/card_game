<?php

/* ::footer.html.twig */
class __TwigTemplate_f7ed70ef476814423d5a38de254ec52c905930b01eeaf4a47c782a07326b20de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ace397924e8b156081e2626c099884ba48f6d2c0067a731570468c85a96f7ff1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ace397924e8b156081e2626c099884ba48f6d2c0067a731570468c85a96f7ff1->enter($__internal_ace397924e8b156081e2626c099884ba48f6d2c0067a731570468c85a96f7ff1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::footer.html.twig"));

        $__internal_1b1f24ac7ae64e92c7fa45afe3482389dd92a22b9882ac21f27f7e1e4157eb96 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b1f24ac7ae64e92c7fa45afe3482389dd92a22b9882ac21f27f7e1e4157eb96->enter($__internal_1b1f24ac7ae64e92c7fa45afe3482389dd92a22b9882ac21f27f7e1e4157eb96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::footer.html.twig"));

        // line 1
        echo "<script src=\"https://code.jquery.com/jquery.min.js\"></script>
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js\"></script>
<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>

";
        
        $__internal_ace397924e8b156081e2626c099884ba48f6d2c0067a731570468c85a96f7ff1->leave($__internal_ace397924e8b156081e2626c099884ba48f6d2c0067a731570468c85a96f7ff1_prof);

        
        $__internal_1b1f24ac7ae64e92c7fa45afe3482389dd92a22b9882ac21f27f7e1e4157eb96->leave($__internal_1b1f24ac7ae64e92c7fa45afe3482389dd92a22b9882ac21f27f7e1e4157eb96_prof);

    }

    public function getTemplateName()
    {
        return "::footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<script src=\"https://code.jquery.com/jquery.min.js\"></script>
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js\"></script>
<script src=\"{{asset('js/app.js')}}\"></script>

", "::footer.html.twig", "C:\\wamp\\www\\cards\\app/Resources\\views/footer.html.twig");
    }
}
