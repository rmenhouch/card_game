<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_bc986aaac5035fdb46e4469a7c26d94eb0a76c6d22643d6554e812fff43401b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7ebb8e1e6fb9e81868b338ced6ad7bb4d412b9afd19e49dde08ae0319516ee8d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ebb8e1e6fb9e81868b338ced6ad7bb4d412b9afd19e49dde08ae0319516ee8d->enter($__internal_7ebb8e1e6fb9e81868b338ced6ad7bb4d412b9afd19e49dde08ae0319516ee8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_8bc85edf3f5ead11f9f760885bfcc09e45d87cd592490b2d8afa27c09306745b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8bc85edf3f5ead11f9f760885bfcc09e45d87cd592490b2d8afa27c09306745b->enter($__internal_8bc85edf3f5ead11f9f760885bfcc09e45d87cd592490b2d8afa27c09306745b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7ebb8e1e6fb9e81868b338ced6ad7bb4d412b9afd19e49dde08ae0319516ee8d->leave($__internal_7ebb8e1e6fb9e81868b338ced6ad7bb4d412b9afd19e49dde08ae0319516ee8d_prof);

        
        $__internal_8bc85edf3f5ead11f9f760885bfcc09e45d87cd592490b2d8afa27c09306745b->leave($__internal_8bc85edf3f5ead11f9f760885bfcc09e45d87cd592490b2d8afa27c09306745b_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_7dcfa771e889424d4061506bb8cd26c8b13e0b40192b99bb58bf2248057b23ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7dcfa771e889424d4061506bb8cd26c8b13e0b40192b99bb58bf2248057b23ca->enter($__internal_7dcfa771e889424d4061506bb8cd26c8b13e0b40192b99bb58bf2248057b23ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_cb04a38b6147bdb6009043839b9237e5f9641958a2bcf99d3f083b1660062607 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb04a38b6147bdb6009043839b9237e5f9641958a2bcf99d3f083b1660062607->enter($__internal_cb04a38b6147bdb6009043839b9237e5f9641958a2bcf99d3f083b1660062607_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_cb04a38b6147bdb6009043839b9237e5f9641958a2bcf99d3f083b1660062607->leave($__internal_cb04a38b6147bdb6009043839b9237e5f9641958a2bcf99d3f083b1660062607_prof);

        
        $__internal_7dcfa771e889424d4061506bb8cd26c8b13e0b40192b99bb58bf2248057b23ca->leave($__internal_7dcfa771e889424d4061506bb8cd26c8b13e0b40192b99bb58bf2248057b23ca_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_6ca103dcda1312d8299a39afa5b01f6e3565fdd7c9a2a17a540bf2c5dc04223c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6ca103dcda1312d8299a39afa5b01f6e3565fdd7c9a2a17a540bf2c5dc04223c->enter($__internal_6ca103dcda1312d8299a39afa5b01f6e3565fdd7c9a2a17a540bf2c5dc04223c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_ee7418fe307b6a29c45179584de57a13f33dd39baa01103ab98ccc054888b82f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ee7418fe307b6a29c45179584de57a13f33dd39baa01103ab98ccc054888b82f->enter($__internal_ee7418fe307b6a29c45179584de57a13f33dd39baa01103ab98ccc054888b82f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_ee7418fe307b6a29c45179584de57a13f33dd39baa01103ab98ccc054888b82f->leave($__internal_ee7418fe307b6a29c45179584de57a13f33dd39baa01103ab98ccc054888b82f_prof);

        
        $__internal_6ca103dcda1312d8299a39afa5b01f6e3565fdd7c9a2a17a540bf2c5dc04223c->leave($__internal_6ca103dcda1312d8299a39afa5b01f6e3565fdd7c9a2a17a540bf2c5dc04223c_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_cfe2378904b66b63d94e782c6a3effbad7d01759ccf6c817963dc14c1e8d7ab7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cfe2378904b66b63d94e782c6a3effbad7d01759ccf6c817963dc14c1e8d7ab7->enter($__internal_cfe2378904b66b63d94e782c6a3effbad7d01759ccf6c817963dc14c1e8d7ab7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_39f0bb9ec35a45aaa60213e78afd1dc55563c410b55fa88d474efaf86ed20b42 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_39f0bb9ec35a45aaa60213e78afd1dc55563c410b55fa88d474efaf86ed20b42->enter($__internal_39f0bb9ec35a45aaa60213e78afd1dc55563c410b55fa88d474efaf86ed20b42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_39f0bb9ec35a45aaa60213e78afd1dc55563c410b55fa88d474efaf86ed20b42->leave($__internal_39f0bb9ec35a45aaa60213e78afd1dc55563c410b55fa88d474efaf86ed20b42_prof);

        
        $__internal_cfe2378904b66b63d94e782c6a3effbad7d01759ccf6c817963dc14c1e8d7ab7->leave($__internal_cfe2378904b66b63d94e782c6a3effbad7d01759ccf6c817963dc14c1e8d7ab7_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\wamp\\www\\cards\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
