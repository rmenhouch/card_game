<?php

/* default/index.html.twig */
class __TwigTemplate_a9b82ab490e695a6b0ba387d8a56aecb2f2b832434a2c804c5dd693943d495f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cd768ec3e3bbf0c4314fe80769bb9ddafaff1e6d4b1f48c14bab5c132b3a85c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cd768ec3e3bbf0c4314fe80769bb9ddafaff1e6d4b1f48c14bab5c132b3a85c8->enter($__internal_cd768ec3e3bbf0c4314fe80769bb9ddafaff1e6d4b1f48c14bab5c132b3a85c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $__internal_a76957a402c56aabeb7d0ecb0d8073f1a255dbb43245cdae5c5b0e9a9e9bf593 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a76957a402c56aabeb7d0ecb0d8073f1a255dbb43245cdae5c5b0e9a9e9bf593->enter($__internal_a76957a402c56aabeb7d0ecb0d8073f1a255dbb43245cdae5c5b0e9a9e9bf593_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cd768ec3e3bbf0c4314fe80769bb9ddafaff1e6d4b1f48c14bab5c132b3a85c8->leave($__internal_cd768ec3e3bbf0c4314fe80769bb9ddafaff1e6d4b1f48c14bab5c132b3a85c8_prof);

        
        $__internal_a76957a402c56aabeb7d0ecb0d8073f1a255dbb43245cdae5c5b0e9a9e9bf593->leave($__internal_a76957a402c56aabeb7d0ecb0d8073f1a255dbb43245cdae5c5b0e9a9e9bf593_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_f3995340eb6aa6c2ce00fbc2158c8f88327416df36f0fb5ddc4064ab51178ec4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f3995340eb6aa6c2ce00fbc2158c8f88327416df36f0fb5ddc4064ab51178ec4->enter($__internal_f3995340eb6aa6c2ce00fbc2158c8f88327416df36f0fb5ddc4064ab51178ec4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_dce9a89383a86f1785f88530051832f75d7a7956481d414c51a190ead6981779 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dce9a89383a86f1785f88530051832f75d7a7956481d414c51a190ead6981779->enter($__internal_dce9a89383a86f1785f88530051832f75d7a7956481d414c51a190ead6981779_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div id=\"wrapper\">
        <div id=\"container\">
            ";
        // line 5
        $this->loadTemplate("::header.html.twig", "default/index.html.twig", 5)->display($context);
        // line 6
        echo "            <!-- Indicators
            ================================================== -->
            <div class=\"bs-docs-section\">
                <div class=\"row\">
                    <div class=\"col-lg-12\">
                        ";
        // line 11
        if (twig_test_empty(($context["cards"] ?? $this->getContext($context, "cards")))) {
            // line 12
            echo "
                            <div class=\"bs-component\">
                                <div class=\"alert alert-dismissible alert-warning\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                                    <h4> Your need to retrieve 10 cards !</h4>
                                    <p>Please click on the <b>GET 10 CARDS</b> button to continue.</p>
                                </div>
                            </div>
                        ";
        } else {
            // line 21
            echo "                           ";
            if (($this->getAttribute(($context["rules"] ?? $this->getContext($context, "rules")), "handSorted", array()) == false)) {
                // line 22
                echo "                            <div class=\"bs-component\">
                                <div class=\"alert alert-dismissible alert-success\" role=\"alert\">
                                    <strong>Good Luck !</strong>
                                    <p>To sort cards, please click the <b>AUTO SORT</b> button.</p>
                                    <p>If you need to match results with excercice solution, please click the <b>VERIFY</b> button.</p>
                                </div>
                            </div>
                           ";
            } elseif (($this->getAttribute(            // line 29
($context["rules"] ?? $this->getContext($context, "rules")), "handSorted", array()) == true)) {
                // line 30
                echo "                                <div class=\"bs-component\">
                                    <div class=\"alert alert-dismissible alert-warning\">
                                        <strong>Sorting done !!</strong>
                                        <p><img class=\"sign-image\" src=\"";
                // line 33
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/signs/DOLLAR.png"), "html", null, true);
                echo "\" /> Cards are Sorted according to required order.</p>
                                        <p>If you need to match results with excercice solution, please click the <b>VERIFY</b> button.</p>
                                    </div>
                                </div>
                            ";
            }
            // line 38
            echo "                            <div class=\"sign-container row centered text-center\">
                                ";
            // line 39
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["rules"] ?? $this->getContext($context, "rules")), "categoryOrder", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 40
                echo "                                    <div class=\"col-lg-3 col-md-3 col-sm-3\">
                                        <img class=\"sign-image\" src=\"";
                // line 41
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl((("images/signs/" . $context["category"]) . ".png")), "html", null, true);
                echo "\" />
                                    </div>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "                                <small class=\"hidden-xs\">";
            echo twig_escape_filter($this->env, twig_join_filter($this->getAttribute(($context["rules"] ?? $this->getContext($context, "rules")), "valueOrder", array()), " >> "), "html", null, true);
            echo "</small>
                            </div>
                            <div class=\"row cards-container\">
                                <div class=\"cards-btn\">
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"order_cards_btn\" ";
            // line 49
            if (($this->getAttribute(($context["rules"] ?? $this->getContext($context, "rules")), "handSorted", array()) == true)) {
                echo " disabled ";
            }
            echo " class=\"btn btn-danger\"><i class=\"fa fa-heart\"></i> AUTO SORT</a>
                                    </div>
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"verify_cards_btn\" ";
            // line 52
            if (($this->getAttribute(($context["rules"] ?? $this->getContext($context, "rules")), "handSorted", array()) == false)) {
                echo " disabled ";
            }
            echo " class=\"btn btn-warning\"><i class=\"fa fa-check\"></i> VERIFY ORDER</a>
                                    </div>
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"refresh_cards_btn\" ";
            // line 55
            if (twig_test_empty(($context["cards"] ?? $this->getContext($context, "cards")))) {
                echo " disabled ";
            }
            echo " class=\"btn btn-info\"><i class=\"fa fa-refresh\"></i> RESTART</a>
                                    </div>
                                    <div class=\"clearfix\"></div>
                                </div>
                                <div>
                                    ";
            // line 60
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["cards"] ?? $this->getContext($context, "cards")));
            foreach ($context['_seq'] as $context["_key"] => $context["card"]) {
                // line 61
                echo "                                        <div class=\"card-container col-lg-2 col-md-5 col-sm-3\">
                                            <img class=\"card-image\" src=\"";
                // line 62
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl((((("images/cards/" . $this->getAttribute($context["card"], "category", array())) . "/") . $this->getAttribute($context["card"], "value", array())) . ".png")), "html", null, true);
                echo "\" />
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['card'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "                                </div>
                            </div>
                        ";
        }
        // line 68
        echo "                    </div>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_dce9a89383a86f1785f88530051832f75d7a7956481d414c51a190ead6981779->leave($__internal_dce9a89383a86f1785f88530051832f75d7a7956481d414c51a190ead6981779_prof);

        
        $__internal_f3995340eb6aa6c2ce00fbc2158c8f88327416df36f0fb5ddc4064ab51178ec4->leave($__internal_f3995340eb6aa6c2ce00fbc2158c8f88327416df36f0fb5ddc4064ab51178ec4_prof);

    }

    // line 74
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_326fb0f6ce563d1a0349df462f7247fac5d1d18e2f7c3ee50ee3aed9f28da7ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_326fb0f6ce563d1a0349df462f7247fac5d1d18e2f7c3ee50ee3aed9f28da7ef->enter($__internal_326fb0f6ce563d1a0349df462f7247fac5d1d18e2f7c3ee50ee3aed9f28da7ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_63ea1483ca8283c1980149dee62d889359ce9aaaa9869fb298e104636607c0eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63ea1483ca8283c1980149dee62d889359ce9aaaa9869fb298e104636607c0eb->enter($__internal_63ea1483ca8283c1980149dee62d889359ce9aaaa9869fb298e104636607c0eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 75
        echo "
    ";
        // line 76
        $this->loadTemplate("::footer.html.twig", "default/index.html.twig", 76)->display($context);
        
        $__internal_63ea1483ca8283c1980149dee62d889359ce9aaaa9869fb298e104636607c0eb->leave($__internal_63ea1483ca8283c1980149dee62d889359ce9aaaa9869fb298e104636607c0eb_prof);

        
        $__internal_326fb0f6ce563d1a0349df462f7247fac5d1d18e2f7c3ee50ee3aed9f28da7ef->leave($__internal_326fb0f6ce563d1a0349df462f7247fac5d1d18e2f7c3ee50ee3aed9f28da7ef_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 76,  202 => 75,  193 => 74,  178 => 68,  173 => 65,  164 => 62,  161 => 61,  157 => 60,  147 => 55,  139 => 52,  131 => 49,  122 => 44,  113 => 41,  110 => 40,  106 => 39,  103 => 38,  95 => 33,  90 => 30,  88 => 29,  79 => 22,  76 => 21,  65 => 12,  63 => 11,  56 => 6,  54 => 5,  50 => 3,  41 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block body %}
    <div id=\"wrapper\">
        <div id=\"container\">
            {% include '::header.html.twig' %}
            <!-- Indicators
            ================================================== -->
            <div class=\"bs-docs-section\">
                <div class=\"row\">
                    <div class=\"col-lg-12\">
                        {% if cards is empty %}

                            <div class=\"bs-component\">
                                <div class=\"alert alert-dismissible alert-warning\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                                    <h4> Your need to retrieve 10 cards !</h4>
                                    <p>Please click on the <b>GET 10 CARDS</b> button to continue.</p>
                                </div>
                            </div>
                        {% else %}
                           {% if rules.handSorted == false %}
                            <div class=\"bs-component\">
                                <div class=\"alert alert-dismissible alert-success\" role=\"alert\">
                                    <strong>Good Luck !</strong>
                                    <p>To sort cards, please click the <b>AUTO SORT</b> button.</p>
                                    <p>If you need to match results with excercice solution, please click the <b>VERIFY</b> button.</p>
                                </div>
                            </div>
                           {% elseif  rules.handSorted == true %}
                                <div class=\"bs-component\">
                                    <div class=\"alert alert-dismissible alert-warning\">
                                        <strong>Sorting done !!</strong>
                                        <p><img class=\"sign-image\" src=\"{{ asset('images/signs/DOLLAR.png') }}\" /> Cards are Sorted according to required order.</p>
                                        <p>If you need to match results with excercice solution, please click the <b>VERIFY</b> button.</p>
                                    </div>
                                </div>
                            {% endif %}
                            <div class=\"sign-container row centered text-center\">
                                {% for category in rules.categoryOrder %}
                                    <div class=\"col-lg-3 col-md-3 col-sm-3\">
                                        <img class=\"sign-image\" src=\"{{ asset('images/signs/'~category~'.png') }}\" />
                                    </div>
                                {% endfor %}
                                <small class=\"hidden-xs\">{{ rules.valueOrder|join(' >> ') }}</small>
                            </div>
                            <div class=\"row cards-container\">
                                <div class=\"cards-btn\">
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"order_cards_btn\" {% if rules.handSorted == true %} disabled {% endif %} class=\"btn btn-danger\"><i class=\"fa fa-heart\"></i> AUTO SORT</a>
                                    </div>
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"verify_cards_btn\" {% if rules.handSorted == false %} disabled {% endif %} class=\"btn btn-warning\"><i class=\"fa fa-check\"></i> VERIFY ORDER</a>
                                    </div>
                                    <div class=\"col-lg-4 col-md-5 col-sm-6\">
                                        <a id=\"refresh_cards_btn\" {% if cards is  empty %} disabled {% endif %} class=\"btn btn-info\"><i class=\"fa fa-refresh\"></i> RESTART</a>
                                    </div>
                                    <div class=\"clearfix\"></div>
                                </div>
                                <div>
                                    {% for card in cards %}
                                        <div class=\"card-container col-lg-2 col-md-5 col-sm-3\">
                                            <img class=\"card-image\" src=\"{{ asset('images/cards/'~card.category~'/'~card.value~'.png') }}\" />
                                        </div>
                                    {% endfor %}
                                </div>
                            </div>
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
{% block javascripts  %}

    {% include '::footer.html.twig' %}
{% endblock %}
", "default/index.html.twig", "C:\\wamp\\www\\cards\\app\\Resources\\views\\default\\index.html.twig");
    }
}
