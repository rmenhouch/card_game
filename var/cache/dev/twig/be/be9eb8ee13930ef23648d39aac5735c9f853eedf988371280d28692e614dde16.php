<?php

/* ::footer.html.twig */
class __TwigTemplate_acd428a6cc84faa9c693b2d9c53968fbbbc949d7b5a5cc34d13c61be06a47de8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_827edf5573e5aef3a2131621ae6161b456be7122a5b91bb529297b5516b712b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_827edf5573e5aef3a2131621ae6161b456be7122a5b91bb529297b5516b712b0->enter($__internal_827edf5573e5aef3a2131621ae6161b456be7122a5b91bb529297b5516b712b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::footer.html.twig"));

        $__internal_eac298906d39b4c45cb9e93619c608241d2740515e26389901ec6b9c98842e30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eac298906d39b4c45cb9e93619c608241d2740515e26389901ec6b9c98842e30->enter($__internal_eac298906d39b4c45cb9e93619c608241d2740515e26389901ec6b9c98842e30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::footer.html.twig"));

        // line 1
        echo "<script src=\"https://code.jquery.com/jquery.min.js\"></script>
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js\"></script>
<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>

";
        
        $__internal_827edf5573e5aef3a2131621ae6161b456be7122a5b91bb529297b5516b712b0->leave($__internal_827edf5573e5aef3a2131621ae6161b456be7122a5b91bb529297b5516b712b0_prof);

        
        $__internal_eac298906d39b4c45cb9e93619c608241d2740515e26389901ec6b9c98842e30->leave($__internal_eac298906d39b4c45cb9e93619c608241d2740515e26389901ec6b9c98842e30_prof);

    }

    public function getTemplateName()
    {
        return "::footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<script src=\"https://code.jquery.com/jquery.min.js\"></script>
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js\"></script>
<script src=\"{{asset('js/app.js')}}\"></script>

", "::footer.html.twig", "C:\\wamp\\www\\cards\\app/Resources\\views/footer.html.twig");
    }
}
