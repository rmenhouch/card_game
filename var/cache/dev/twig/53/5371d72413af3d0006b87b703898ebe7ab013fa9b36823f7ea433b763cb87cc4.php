<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_d1b1366034bbb21ae068b63aa0980f98d48b3e9fa416a297295a0fdc77208685 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c7a025227094606233f67138abe7542634e5c262ec0ac06aa7f625e0ec10662a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c7a025227094606233f67138abe7542634e5c262ec0ac06aa7f625e0ec10662a->enter($__internal_c7a025227094606233f67138abe7542634e5c262ec0ac06aa7f625e0ec10662a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_0bb8746b07759d21fa75f81e1d151553b7eda32de20c02e9cc5b7287f10c0dc6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0bb8746b07759d21fa75f81e1d151553b7eda32de20c02e9cc5b7287f10c0dc6->enter($__internal_0bb8746b07759d21fa75f81e1d151553b7eda32de20c02e9cc5b7287f10c0dc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c7a025227094606233f67138abe7542634e5c262ec0ac06aa7f625e0ec10662a->leave($__internal_c7a025227094606233f67138abe7542634e5c262ec0ac06aa7f625e0ec10662a_prof);

        
        $__internal_0bb8746b07759d21fa75f81e1d151553b7eda32de20c02e9cc5b7287f10c0dc6->leave($__internal_0bb8746b07759d21fa75f81e1d151553b7eda32de20c02e9cc5b7287f10c0dc6_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_7e4af3ffe5a5e58c6720170dbb5288bf88ed12fa474862ae9531775dd2bfdff4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e4af3ffe5a5e58c6720170dbb5288bf88ed12fa474862ae9531775dd2bfdff4->enter($__internal_7e4af3ffe5a5e58c6720170dbb5288bf88ed12fa474862ae9531775dd2bfdff4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_4f040049f9e45142989d15c66ee9486120c9cba1ddee2673705d52fbe47d280e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f040049f9e45142989d15c66ee9486120c9cba1ddee2673705d52fbe47d280e->enter($__internal_4f040049f9e45142989d15c66ee9486120c9cba1ddee2673705d52fbe47d280e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_4f040049f9e45142989d15c66ee9486120c9cba1ddee2673705d52fbe47d280e->leave($__internal_4f040049f9e45142989d15c66ee9486120c9cba1ddee2673705d52fbe47d280e_prof);

        
        $__internal_7e4af3ffe5a5e58c6720170dbb5288bf88ed12fa474862ae9531775dd2bfdff4->leave($__internal_7e4af3ffe5a5e58c6720170dbb5288bf88ed12fa474862ae9531775dd2bfdff4_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_f2bb535eb3a52b8ea98796073399d83a2ee9aa831595bbd6ac0a0eda6b82219f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f2bb535eb3a52b8ea98796073399d83a2ee9aa831595bbd6ac0a0eda6b82219f->enter($__internal_f2bb535eb3a52b8ea98796073399d83a2ee9aa831595bbd6ac0a0eda6b82219f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_8b4d397936afe00e3109fd81de1a9fb21bd74c7a2f663a75b01516c98b7f257e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b4d397936afe00e3109fd81de1a9fb21bd74c7a2f663a75b01516c98b7f257e->enter($__internal_8b4d397936afe00e3109fd81de1a9fb21bd74c7a2f663a75b01516c98b7f257e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_8b4d397936afe00e3109fd81de1a9fb21bd74c7a2f663a75b01516c98b7f257e->leave($__internal_8b4d397936afe00e3109fd81de1a9fb21bd74c7a2f663a75b01516c98b7f257e_prof);

        
        $__internal_f2bb535eb3a52b8ea98796073399d83a2ee9aa831595bbd6ac0a0eda6b82219f->leave($__internal_f2bb535eb3a52b8ea98796073399d83a2ee9aa831595bbd6ac0a0eda6b82219f_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_2a9eabf86ebd4aa0ae836f5348fd5339ecd7d23e16057c6e952080cb9151064c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a9eabf86ebd4aa0ae836f5348fd5339ecd7d23e16057c6e952080cb9151064c->enter($__internal_2a9eabf86ebd4aa0ae836f5348fd5339ecd7d23e16057c6e952080cb9151064c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_622e0b0ce50b7d237362e45d7ae2d633378a601e017189f548ca1e6bc4248561 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_622e0b0ce50b7d237362e45d7ae2d633378a601e017189f548ca1e6bc4248561->enter($__internal_622e0b0ce50b7d237362e45d7ae2d633378a601e017189f548ca1e6bc4248561_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_622e0b0ce50b7d237362e45d7ae2d633378a601e017189f548ca1e6bc4248561->leave($__internal_622e0b0ce50b7d237362e45d7ae2d633378a601e017189f548ca1e6bc4248561_prof);

        
        $__internal_2a9eabf86ebd4aa0ae836f5348fd5339ecd7d23e16057c6e952080cb9151064c->leave($__internal_2a9eabf86ebd4aa0ae836f5348fd5339ecd7d23e16057c6e952080cb9151064c_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\wamp\\www\\cards\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
