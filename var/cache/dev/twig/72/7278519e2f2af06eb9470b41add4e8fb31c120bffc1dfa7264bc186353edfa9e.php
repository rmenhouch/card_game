<?php

/* base.html.twig */
class __TwigTemplate_b86e9f7a511a763042c29b9d741ffd87fe1350995e8f47bf47ec08c4c5900a0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b640533fd53ca63d3201ebe6566c5c307ca3671a39f5770d85b33826c86928af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b640533fd53ca63d3201ebe6566c5c307ca3671a39f5770d85b33826c86928af->enter($__internal_b640533fd53ca63d3201ebe6566c5c307ca3671a39f5770d85b33826c86928af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_2773e67b1899abfbcd731e76959de0be49a8a506a61770ac9197ed2ad4dea316 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2773e67b1899abfbcd731e76959de0be49a8a506a61770ac9197ed2ad4dea316->enter($__internal_2773e67b1899abfbcd731e76959de0be49a8a506a61770ac9197ed2ad4dea316_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_b640533fd53ca63d3201ebe6566c5c307ca3671a39f5770d85b33826c86928af->leave($__internal_b640533fd53ca63d3201ebe6566c5c307ca3671a39f5770d85b33826c86928af_prof);

        
        $__internal_2773e67b1899abfbcd731e76959de0be49a8a506a61770ac9197ed2ad4dea316->leave($__internal_2773e67b1899abfbcd731e76959de0be49a8a506a61770ac9197ed2ad4dea316_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_c767c9299427428be357f9d6bc401a05b410c22644b95fbd57e0b0a62768484b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c767c9299427428be357f9d6bc401a05b410c22644b95fbd57e0b0a62768484b->enter($__internal_c767c9299427428be357f9d6bc401a05b410c22644b95fbd57e0b0a62768484b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_ef4d7928f15049c9138790d28dce771f935bdf10f5778e0ce8267c9e2d3693cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef4d7928f15049c9138790d28dce771f935bdf10f5778e0ce8267c9e2d3693cf->enter($__internal_ef4d7928f15049c9138790d28dce771f935bdf10f5778e0ce8267c9e2d3693cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "G@@D Luck!";
        
        $__internal_ef4d7928f15049c9138790d28dce771f935bdf10f5778e0ce8267c9e2d3693cf->leave($__internal_ef4d7928f15049c9138790d28dce771f935bdf10f5778e0ce8267c9e2d3693cf_prof);

        
        $__internal_c767c9299427428be357f9d6bc401a05b410c22644b95fbd57e0b0a62768484b->leave($__internal_c767c9299427428be357f9d6bc401a05b410c22644b95fbd57e0b0a62768484b_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_fa290c9196973ae6c00e63f4a92f484314a1a4d2695c3b8a929587892fb67a8e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa290c9196973ae6c00e63f4a92f484314a1a4d2695c3b8a929587892fb67a8e->enter($__internal_fa290c9196973ae6c00e63f4a92f484314a1a4d2695c3b8a929587892fb67a8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_76ad375416094a68a3aa9b3b1760a4df601ffd38e137a02d2338f5ea03dc653e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_76ad375416094a68a3aa9b3b1760a4df601ffd38e137a02d2338f5ea03dc653e->enter($__internal_76ad375416094a68a3aa9b3b1760a4df601ffd38e137a02d2338f5ea03dc653e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_76ad375416094a68a3aa9b3b1760a4df601ffd38e137a02d2338f5ea03dc653e->leave($__internal_76ad375416094a68a3aa9b3b1760a4df601ffd38e137a02d2338f5ea03dc653e_prof);

        
        $__internal_fa290c9196973ae6c00e63f4a92f484314a1a4d2695c3b8a929587892fb67a8e->leave($__internal_fa290c9196973ae6c00e63f4a92f484314a1a4d2695c3b8a929587892fb67a8e_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_dbd96baf9414fc88dff0c555b5fd95df8bbdec513490adce5fd4cbcba8fe3311 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dbd96baf9414fc88dff0c555b5fd95df8bbdec513490adce5fd4cbcba8fe3311->enter($__internal_dbd96baf9414fc88dff0c555b5fd95df8bbdec513490adce5fd4cbcba8fe3311_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_aba156b53d20464d90db0ab6bdddc59a2710c7f07bf328318ec3bbc93137a842 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aba156b53d20464d90db0ab6bdddc59a2710c7f07bf328318ec3bbc93137a842->enter($__internal_aba156b53d20464d90db0ab6bdddc59a2710c7f07bf328318ec3bbc93137a842_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_aba156b53d20464d90db0ab6bdddc59a2710c7f07bf328318ec3bbc93137a842->leave($__internal_aba156b53d20464d90db0ab6bdddc59a2710c7f07bf328318ec3bbc93137a842_prof);

        
        $__internal_dbd96baf9414fc88dff0c555b5fd95df8bbdec513490adce5fd4cbcba8fe3311->leave($__internal_dbd96baf9414fc88dff0c555b5fd95df8bbdec513490adce5fd4cbcba8fe3311_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_340f1eb6fed3d86b5aa60f054b99015acbbeedbf694da7bd078069761b9a286a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_340f1eb6fed3d86b5aa60f054b99015acbbeedbf694da7bd078069761b9a286a->enter($__internal_340f1eb6fed3d86b5aa60f054b99015acbbeedbf694da7bd078069761b9a286a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_455d869ffb4ba00093bf5e1d29339e49a69e8292502076f1ec40b638476b35aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_455d869ffb4ba00093bf5e1d29339e49a69e8292502076f1ec40b638476b35aa->enter($__internal_455d869ffb4ba00093bf5e1d29339e49a69e8292502076f1ec40b638476b35aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_455d869ffb4ba00093bf5e1d29339e49a69e8292502076f1ec40b638476b35aa->leave($__internal_455d869ffb4ba00093bf5e1d29339e49a69e8292502076f1ec40b638476b35aa_prof);

        
        $__internal_340f1eb6fed3d86b5aa60f054b99015acbbeedbf694da7bd078069761b9a286a->leave($__internal_340f1eb6fed3d86b5aa60f054b99015acbbeedbf694da7bd078069761b9a286a_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}G@@D Luck!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "C:\\wamp\\www\\cards\\app\\Resources\\views\\base.html.twig");
    }
}
