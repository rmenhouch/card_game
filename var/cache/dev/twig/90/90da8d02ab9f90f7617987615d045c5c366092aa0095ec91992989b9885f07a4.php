<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_0fd258d5cd90e6569d1ab61f7a8b5064101977573920a7e0c68993aed9a371c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ff9fabaf920caa5e8309d7dad2b596e26e0ab248b0cf14f06659b73257ad49f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6ff9fabaf920caa5e8309d7dad2b596e26e0ab248b0cf14f06659b73257ad49f->enter($__internal_6ff9fabaf920caa5e8309d7dad2b596e26e0ab248b0cf14f06659b73257ad49f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_b59f144ba617fe8b33f0da5a28c46b5848c28ef5aa76e1aa0ecbd85f4a50b8b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b59f144ba617fe8b33f0da5a28c46b5848c28ef5aa76e1aa0ecbd85f4a50b8b0->enter($__internal_b59f144ba617fe8b33f0da5a28c46b5848c28ef5aa76e1aa0ecbd85f4a50b8b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6ff9fabaf920caa5e8309d7dad2b596e26e0ab248b0cf14f06659b73257ad49f->leave($__internal_6ff9fabaf920caa5e8309d7dad2b596e26e0ab248b0cf14f06659b73257ad49f_prof);

        
        $__internal_b59f144ba617fe8b33f0da5a28c46b5848c28ef5aa76e1aa0ecbd85f4a50b8b0->leave($__internal_b59f144ba617fe8b33f0da5a28c46b5848c28ef5aa76e1aa0ecbd85f4a50b8b0_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_0a44d383c170961d13b75c9521c59b7df05d40d181129422912afd6255104f72 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a44d383c170961d13b75c9521c59b7df05d40d181129422912afd6255104f72->enter($__internal_0a44d383c170961d13b75c9521c59b7df05d40d181129422912afd6255104f72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_8e9bd2d43cb8bf888e32862c1d5308a145336472b09ff7467b154d6bc95eb4e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e9bd2d43cb8bf888e32862c1d5308a145336472b09ff7467b154d6bc95eb4e0->enter($__internal_8e9bd2d43cb8bf888e32862c1d5308a145336472b09ff7467b154d6bc95eb4e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_8e9bd2d43cb8bf888e32862c1d5308a145336472b09ff7467b154d6bc95eb4e0->leave($__internal_8e9bd2d43cb8bf888e32862c1d5308a145336472b09ff7467b154d6bc95eb4e0_prof);

        
        $__internal_0a44d383c170961d13b75c9521c59b7df05d40d181129422912afd6255104f72->leave($__internal_0a44d383c170961d13b75c9521c59b7df05d40d181129422912afd6255104f72_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_f3900cfbfec6b375cc2cd13539c43ba25154abb5d302b174b77c5bb9adda0ee2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f3900cfbfec6b375cc2cd13539c43ba25154abb5d302b174b77c5bb9adda0ee2->enter($__internal_f3900cfbfec6b375cc2cd13539c43ba25154abb5d302b174b77c5bb9adda0ee2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_deda86b8e5c7baf745760dd3a8765161ad6365fed44174b399f1495738b834bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_deda86b8e5c7baf745760dd3a8765161ad6365fed44174b399f1495738b834bf->enter($__internal_deda86b8e5c7baf745760dd3a8765161ad6365fed44174b399f1495738b834bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_deda86b8e5c7baf745760dd3a8765161ad6365fed44174b399f1495738b834bf->leave($__internal_deda86b8e5c7baf745760dd3a8765161ad6365fed44174b399f1495738b834bf_prof);

        
        $__internal_f3900cfbfec6b375cc2cd13539c43ba25154abb5d302b174b77c5bb9adda0ee2->leave($__internal_f3900cfbfec6b375cc2cd13539c43ba25154abb5d302b174b77c5bb9adda0ee2_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_112e64199b937b7b8194f7897ab577772eef51797bfac5e7e22796c1e524859d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_112e64199b937b7b8194f7897ab577772eef51797bfac5e7e22796c1e524859d->enter($__internal_112e64199b937b7b8194f7897ab577772eef51797bfac5e7e22796c1e524859d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_28651727c8bc8ba967dfb5058bd68397bc47e4aff4800745c6b85744ea8f2dd2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28651727c8bc8ba967dfb5058bd68397bc47e4aff4800745c6b85744ea8f2dd2->enter($__internal_28651727c8bc8ba967dfb5058bd68397bc47e4aff4800745c6b85744ea8f2dd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_28651727c8bc8ba967dfb5058bd68397bc47e4aff4800745c6b85744ea8f2dd2->leave($__internal_28651727c8bc8ba967dfb5058bd68397bc47e4aff4800745c6b85744ea8f2dd2_prof);

        
        $__internal_112e64199b937b7b8194f7897ab577772eef51797bfac5e7e22796c1e524859d->leave($__internal_112e64199b937b7b8194f7897ab577772eef51797bfac5e7e22796c1e524859d_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\wamp\\www\\cards\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
